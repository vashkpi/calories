﻿
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Не ЗначениеЗаполнено(Объект.Ссылка) Тогда
		
		Объект.День 								= НачалоДня(ТекущаяДата());
		Объект.МаксимальноеКоличествоКалорийДень 	= Объект.Человек.МаксимальноеКоличествоКалорийДень;
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПродуктыПродуктПриИзменении(Элемент)
	
	РаботаСТабличнойЧастьюКлиент.ПродуктПриИзменении(ЭтаФорма, Элемент);	
		
КонецПроцедуры

&НаКлиенте
Процедура ПродуктыКалорийВЕдиницеПриИзменении(Элемент)
	
	РаботаСТабличнойЧастьюКлиент.РассчитатьКоличествоКалорийВСтроке(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ПродуктыВесПриИзменении(Элемент)
	
	РаботаСТабличнойЧастьюКлиент.РассчитатьКоличествоКалорийВСтроке(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ПродуктыКалорийПриИзменении(Элемент)
	
	РаботаСТабличнойЧастьюКлиент.РассчитатьВесВСтроке(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ПродуктыПриОкончанииРедактирования(Элемент, НоваяСтрока, ОтменаРедактирования)
	
	РаботаСТабличнойЧастьюКлиент.РассчитатьИтогоКалорий(ЭтаФорма, "Калорий");
	
	Раскрасить();
	
КонецПроцедуры

&НаСервере
Процедура Раскрасить()
	
	Если Объект.МаксимальноеКоличествоКалорийДень <= Объект.Калорий Тогда
		
		Элементы.МаксКолвоКалорий.ЦветТекстаЗаголовка 	= ЦветаСтиля.ЦветОтрицательногоЧисла;
		Элементы.МаксКолвоКалорий.ЦветТекста 			= ЦветаСтиля.ЦветОтрицательногоЧисла;
		
	Иначе
		
		Элементы.МаксКолвоКалорий.ЦветТекстаЗаголовка 	= ЦветаСтиля.ЦветТекстаФормы;
		Элементы.МаксКолвоКалорий.ЦветТекста 			= ЦветаСтиля.ЦветТекстаФормы;
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ЧеловекПриИзменении(Элемент)
	
	Объект.МаксимальноеКоличествоКалорийДень 	= ПолучитьМаксимальноеКоличествоКалорийПоЧеловеку();
	
КонецПроцедуры

&НаСервере
Функция ПолучитьМаксимальноеКоличествоКалорийПоЧеловеку()
	Возврат Объект.Человек.МаксимальноеКоличествоКалорийДень;	
КонецФункции

